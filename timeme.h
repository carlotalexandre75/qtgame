#ifndef TIMEME_H
#define TIMEME_H

#include <QString>
#include "hud.h"

class timeMe : public HUD
{
    public:
        timeMe(QString txt, int posX, int posY, int sizeFont);
        void increase(int a);
        int mins;
};

#endif // TIMEME_H
