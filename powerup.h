#ifndef POWERUP_H
#define POWERUP_H

#include <QMediaPlayer>
#include <QGraphicsPixmapItem>

class powerUp : public QGraphicsPixmapItem
{
    public:
        int val, type;
        QMediaPlayer *sound;
        powerUp();
        ~powerUp();
};

#endif // POWERUP_H
