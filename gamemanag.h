#ifndef GAMEMANAG_H
#define GAMEMANAG_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QMediaPlayer>
#include <QKeyEvent>
#include <QTimer>
#include <QMovie>

#include "hero.h"
#include "ennemi.h"
#include "golem.h"
#include "orc.h"
#include "timeme.h"

class gameManag : public QGraphicsView{
    Q_OBJECT
    public:
        // Where to shoot
        bool right=false, left=false, up=true, down=false;

        QGraphicsScene *scene;
        timeMe *timer;
        QTimer *uneSec, *timerPower;
        QMediaPlayer * musique;
        hero * link;
        ennemi * ennemi;

        gameManag();
        ~gameManag();
        void playMusique();
        void spawn();
        void keyPressEvent(QKeyEvent *event);
    public slots:
        void createPowerUps();
        void oneSec();
        void endGame(int hp);

    private:
        QMovie * animation;

};

#endif // GAMEMANAG_H
