#include "golem.h"
#include "gamemanag.h"

#include <QGraphicsScene>
#include <QDebug>

extern gameManag * gameManag;

Golem::Golem(): ennemi(5,5,5,false,false,false,false){

    int x, y;
    int hasard1 = (rand() * 4 ) / RAND_MAX + 1;
    int hasardY = (rand() * 800 ) / RAND_MAX ;
    int hasardX = (rand() * 1300 ) / RAND_MAX ;

    if (hasard1 == 1){
        y = 0;
        up = true;
         x= hasardX;
            setPos(x, y);

    }else if (hasard1 == 2) {
        x = 0;
        left = true;
        y = hasardY;
            setPos(x, y);

    }else if (hasard1 == 3) {
        y = 800;
        down = true;
        x = hasardX;
            setPos(x, y);

    }else {
        x = 1320;
        right = true;
        y = hasardY ;
            setPos(x, y);
    }


    if (left) {
        setPixmap(QPixmap(":rsrc/img/iceGolem.png"));
    }
    else if(right || up || down){
        setPixmap(QPixmap(":rsrc/img/iceGolemG.png"));
    }

    QTimer * timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(attaquer()));
    timer->start(50);



}
