#include "menu.h"

bool soundOn = true;
menu::menu(){

    //Set Font
    QFontDatabase::addApplicationFont(":/rsrc/img/orange juice 2.0.ttf");
    QFont font = QFont("Orange Juice",18);
    setFont(font);

    // Set Window
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(1400,900);
    setWindowTitle("Menu");
    // Set Music
    musique = new QMediaPlayer();
    musique->setMedia(QUrl("qrc:/rsrc/music/GameIntro.mp3"));
    musique->play();
    // Set Scene
    scene_menu = new QGraphicsScene();
    scene_menu->setSceneRect(0,0,1400,900);
    scene_menu->setBackgroundBrush(QBrush(QImage(":/rsrc/img/Interface.jpg")));
    // Set text
    QTextEdit * txt = new QTextEdit;
    txt ->setText("Hello");


    // Music on Button
    musiquePlay = new QPushButton("musique On");
    musiquePlay->setGeometry(350,615,310,50);
    musiquePlay->setFont(font);
    musiquePlay->setStyleSheet("color:rgb(109, 21, 7)");
    connect(musiquePlay,SIGNAL(clicked()),this,SLOT(playMusique()));
    scene_menu->addWidget(musiquePlay);
    // Music off Button
    musiqueStop = new QPushButton("musique Off");
    musiqueStop->setFont(font);
    musiqueStop->setGeometry(680,615,310,50);
    musiqueStop->setStyleSheet("color:rgb(109, 21, 7)");
    connect(musiqueStop,SIGNAL(clicked()),this,SLOT(stopMusique()));
    scene_menu->addWidget(musiqueStop);
    // Play Button
    jouer = new QPushButton("Jouer");
    jouer->setGeometry(350,415,640,50);
    jouer->setFont(font);
    jouer->setStyleSheet("color:rgb(109, 21, 7)");
    connect(jouer,SIGNAL(clicked()),this,SLOT(play()));
    scene_menu->addWidget(jouer);
    // Quit Button
    quitter = new QPushButton("Quitter");
    quitter->setGeometry(350,515,640,50);
    quitter->setFont(font);
    quitter->setStyleSheet("color:rgb(109, 21, 7)");
    connect(quitter,SIGNAL(clicked()),this,SLOT(close()));
    scene_menu->addWidget(quitter);
    // Add Logo production
    logo = new QGraphicsPixmapItem();
    logo->setPixmap(QPixmap(":/rsrc/img/logo.png"));
    logo->setPos(0,0);
    scene_menu->addItem(logo);


    setScene(scene_menu);

    //show();
}

void menu::play()
{
    gameWindow = new gameManag();
    musique->stop();
    this -> close();
}

void menu::lose(){
    Perdu = new QGraphicsScene();
    Perdu->setSceneRect(0,0,1400,900);
    Perdu->setBackgroundBrush(QBrush(QImage(":rsrc/img/Map1.png")));
    logoPerdu->setPixmap(QPixmap(":rsrc/img/perdu.png"));
    logoPerdu->setPos(300,100);
    Perdu->addItem(logoPerdu);
    setScene(Perdu);
}

void menu::playMusique(){
    musique->play();
}

void menu::stopMusique(){
    musique->stop();
    soundOn = false;
}
