#include "timeme.h"
#include <QString>

timeMe::timeMe(QString txt, int posX, int posY, int sizeFont)
    : HUD(txt, posX, posY, sizeFont)
{
    // Here value = seconds
    this->mins = 0;
}

void timeMe::increase(int a){
    value += a;
    if (value % 60 == 0) {
        mins++;
    }
    if (mins > 0 && value % 60 == 0)
    {
        value = 0;
        setPlainText(txt + QString::number(mins) + ":" + QString::number(value));
    }
    else {
        setPlainText(txt + QString::number(mins) + ":" + QString::number(value));
    }
}
