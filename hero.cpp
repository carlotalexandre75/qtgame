#include "hero.h"
#include "powerup.h"
#include "bullet.h"
#include "ennemi.h"
#include "gamemanag.h"
#include "orc.h"
#include "golem.h"



hero::hero(int hp_, int dmg_, int spd_){

    hp = hp_;
    dmg = dmg_;
    spd = spd_;


    setFlag(QGraphicsItem::ItemIsFocusable);
    animation=new QMovie(":rsrc/img/hero-run.gif");
    animation->start();
    setPixmap(animation->currentPixmap());

    QTimer *timer = new QTimer();
    timer->start(100);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(isColliding()));
}

hero::~hero() {}

int _hp, _dmg;
QTimer* t = new QTimer();





void hero::isColliding()
{
    QList<QGraphicsItem *> colliding_items = collidingItems();
    for(int i = 0; i < colliding_items.length(); i++)
    {
        powerUp *power = dynamic_cast<powerUp *>(colliding_items[i]);
        Bullet *bullet = dynamic_cast<Bullet *>(colliding_items[i]);
        ennemi *mob = dynamic_cast<ennemi *>(colliding_items[i]);
        if (power)
        {
            switch (power->type) {
            case 1:
            // HP Potion
                hp += 2;
                emit hpChanged(hp);
                break;
            case 2:
            // Invicibility (temporary) + full life
                hp += 10000;
                _hp = 99994;
                connect(t, SIGNAL(timeout()), this, SLOT(rstHp()));
                t->start(5000000);
                emit hpChanged(hp);
                break;
            case 3:
            // Dmg Boost
                dmg += 2;
                _dmg = 2;
                emit dmgChanged(dmg);
                connect(t, SIGNAL(timeout()), this, SLOT(rstDmg()));
                t->start(10000);
                break;
            case 4:
            // Mega dmg boost, temporary
                dmg += 6;           
                _dmg = 6;
                emit dmgChanged(dmg);
                connect(t, SIGNAL(timeout()), this, SLOT(rstDmg()));
                t->start(1000000);
                break;
            case 5:
            // Bomb
                hp -= 3;
                emit hpChanged(hp);
                break;
            }
            scene()->removeItem(power);
            delete power;
        }
        if (bullet)
        {
           hp -= 1;
           emit hpChanged(hp);
           scene()->removeItem(bullet);
           delete bullet;
        }
        if (mob)
        {
            hp -= 2;
            emit hpChanged(hp);

        }
    }
}


void hero::spawn(){
    // Creation d'ennemies

    if(rand() % 2 + 1 == 2){
        Orc * orc = new Orc();
        scene()->addItem(orc);
    }
    else {
        Golem * golem = new Golem();
        scene()->addItem(golem);
    }
}

void hero::rstHp() {
    hp -= _hp;
    t->stop();
    emit hpChanged(hp);
}
void hero::rstDmg() {
    dmg -= _dmg;
    t->stop();
    emit dmgChanged(dmg);
}



