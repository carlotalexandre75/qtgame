#ifndef HERO_H
#define HERO_H


#include <QMediaPlayer>
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QPointF>
#include <QTimer>
#include <QMovie>

class hero: public QObject, public QGraphicsPixmapItem {
    Q_OBJECT

    public:
        hero(int hp, int dmg, int spd);
        ~hero();
        int hp, dmg, spd;
        int getSpd() {return spd;}
        double getX() {return pos().x();}
        double getY() {return pos().y();}

        enum { HeroItemType = UserType + 20 };
           virtual int type() const { return HeroItemType; }


    private:
        QMovie * animation;

public slots:
       void isColliding();
       void rstHp();
       void rstDmg();
       void spawn();

    signals:
       void hpChanged(int a);
       void dmgChanged(int a);
};

#endif // HERO_H
