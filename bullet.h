#ifndef BULLET_H
#define BULLET_H
#include <QGraphicsPixmapItem>
#include <QObject>
#include <string>
#include <QTimer>
#include <QGraphicsScene>

class Bullet: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    public:
        bool right, left, up, down;
        int spd;
        Bullet(int spd, bool right, bool left, bool up,
               bool down);
        ~Bullet();

        enum { BulletItemType = UserType + 20 };
           virtual int type() const { return BulletItemType; }

    public slots:
        void move();
   //     void colliding();



};

#endif // BULLET_H
