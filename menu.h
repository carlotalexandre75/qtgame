#ifndef MENU_H
#define MENU_H

#include "gamemanag.h"

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPushButton>
#include <QGraphicsProxyWidget>
#include <QObject>
#include <QGraphicsItem>
#include<QGraphicsPixmapItem>
#include <QWidget>
#include <QMediaPlayer>
#include <QGraphicsTextItem>
#include <QString>
#include <QTextEdit>

class menu : public QGraphicsView
{
    Q_OBJECT
    public:
        menu();
        QGraphicsScene * scene_menu;
        QGraphicsScene * Perdu;
        QGraphicsPixmapItem * logoPerdu;
        QPushButton * jouer;
        QPushButton * musiquePlay;
        QPushButton * musiqueStop;
        QPushButton * quitter;
        QGraphicsPixmapItem * logo;
        QGraphicsPixmapItem * logo2;
        QMediaPlayer * musique;
        gameManag * gameWindow;

    public slots:
        void lose();
        void play();
        void playMusique();
        void stopMusique();
};

#endif // MENU_H

