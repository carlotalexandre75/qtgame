#include "powerup.h"

powerUp::powerUp(){
    type = rand() * 6 / RAND_MAX +1;
    sound = new QMediaPlayer();
    switch (type) {
        case 1:
        // HP Potion
            sound->setMedia(QUrl("qrc:/music/PikminNiv.mp3"));
            setPixmap(QPixmap(":rsrc/img/coeur.png"));
            setZValue(1);
            break;
        case 2:
        // Invicibility (temporary) + full life
            sound->setMedia(QUrl("qrc:/music/PikminNiv.mp3"));
            setPixmap(QPixmap(":rsrc/img/star.png"));
            setZValue(2);
            break;
        case 3:
        // Dmg Boost
            sound->setMedia(QUrl("qrc:/music/PikminNiv.mp3"));
            setPixmap(QPixmap(":rsrc/img/pink.png"));
            setZValue(3);
            break;
        case 4:
        // Mega dmg boost, temporary
            sound->setMedia(QUrl("qrc:/music/PikminNiv.mp3"));
            setPixmap(QPixmap(":rsrc/img/purple.png"));
            setZValue(4);
            break;
        case 5:
        // Bomb
            sound->setMedia(QUrl("qrc:/music/PikminNiv.mp3"));
            setPixmap(QPixmap(":rsrc/img/bombCoeur.png"));
            setZValue(5);
            break;
    }
    int posX = rand() * 1400 / RAND_MAX;
    int posY = rand() * 900 / RAND_MAX;
    setPos(posX, posY);
}

powerUp::~powerUp() {}

