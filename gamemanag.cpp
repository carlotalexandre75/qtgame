#include "gamemanag.h"
#include "hero.h"
#include "bullet.h"
#include "powerup.h"
#include "ennemi.h"
#include <QDebug>
#include "sprite.h"
#include "timeme.h"


//static hero *link;
extern bool soundOn;


gameManag::gameManag()
{
    if (soundOn)
    {
        playMusique();
    }
    // Window Params
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setWindowTitle("Run but also shoot for your life");
    // Set scene
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,1400,900);
    setFixedSize(1400,900);
    setBackgroundBrush(QBrush(QImage(":rsrc/img/MapFinal.png")));


    // Add Hero (player)
    link = new hero(5,5,20);
    link->setPos(width()/2 - 50 ,height()/2 - 50 );
    scene->addItem(link);

    // Add Ennemi
//    E = new ennemi (5, 5 ,5);
//    scene->addItem(E);


    QTimer * timer1 = new QTimer();
    QObject::connect(timer1, SIGNAL(timeout()), link, SLOT(spawn()));
    timer1->start(1000);









    // Add Score
    HUD *score = new HUD("Killed: ", 1150, 0, 30);
    connect(link,SIGNAL(killChanged(int)), score, SLOT(update(int)));
//    score->increase(1);
    scene->addItem(score);

    // Add Life bar
    HUD *hpBar = new HUD("Vie: ", 50, 0, 30);
    hpBar->increase(link->hp);
    connect(link, SIGNAL(hpChanged(int)), hpBar, SLOT(update(int)));
    scene->addItem(hpBar);

    // Add dmg bar
    HUD *dmgBar = new HUD("DPS: ", 350, 0, 30);
    dmgBar->increase(link->hp);
    connect(link, SIGNAL(dmgChanged(int)), dmgBar, SLOT(update(int)));
    scene->addItem(dmgBar);

    // Add Timer
    timer = new timeMe("Temps  ", 820, 0,  30);
    uneSec = new QTimer();
    connect(uneSec,SIGNAL(timeout()),this,SLOT(oneSec()));
    uneSec->start(1000);
    scene->addItem(timer);

    // Set the powerUp creator
    timerPower = new QTimer();
    connect(timerPower,SIGNAL(timeout()),this,SLOT(createPowerUps()));
    timerPower->start(9000);

    //scene->addItem(new Sprite());       // We put on the scene a new sprite object
    connect(link, SIGNAL(hpChanged(int)),this, SLOT(endGame(int)));
    setScene(scene);
    show();
}

gameManag::~gameManag() {delete this;}



void gameManag::keyPressEvent(QKeyEvent *event){
    if (event->key() == Qt::Key_Left){
        if  (link->x() <= 0 )
            {
                link->setPos(link->getX() , link->getY() );
            }
        else {
       link->setPixmap(QPixmap(":rsrc/img/Fallen_AngelsG.png"));

        link->setPos(link->getX() - link->getSpd(), link->getY());
        left = true;
        right = false;
        up = false;
        down = false;
        }
    }
    else if (event->key() == Qt::Key_Right){
        //bloqué à droite
       if (link->getX() >= 1365)
           {
              link->setPos(link->getX() , link->getY() );
           }
       else {

           link->setPixmap(QPixmap(":rsrc/img/Fallen_Angels.png"));



           link->setPos(link->getX() + link->getSpd(), link->getY());
           right = true;
           left = false;
           up = false;
           down = false;
       }
    }
    else if (event->key() == Qt::Key_Up){
        //bloqué en haut
       if (link->getY() <= 0 )
           {
               link->setPos(link->getX()  ,link->getY() );
           }
       else{
        link->setPos(link->getX(), link->getY() - link->getSpd());
        up = true;
        right = false;
        left = false;
        down = false;
       }
    }
    else if (event->key() == Qt::Key_Down){
        //bloqué en bas
        if (link->getY() >= 865 )
            {
               link->setPos(link->getX()  ,link->getY() );
            }
        else {
        link->setPos(link->getX(), link->getY() + link->getSpd());
        down = true;
        right = false;
        up = false;
        left = false;
        }
    }
    else if (event->key() == Qt::Key_Space){
        Bullet * bullet = new Bullet(12, right, left, up, down);
        if (right) {
            bullet->setPos(link->getX() +60,link-> getY() +30);

        } else if (left){
            bullet->setPos(link->getX() -30, link->getY() +30);

        } else if (up) {
            bullet->setPos(link->getX() +15,link-> getY() -30);

        } else {
            bullet->setPos(link->getX() +15, link->getY() +80);

        }
        bullet->ensureVisible();
        scene->addItem(bullet);


    }
}

void gameManag::createPowerUps(){
    powerUp *random = new powerUp();
    scene->addItem(random);
}

void gameManag::oneSec(){ 
    timer->increase(1);
}

void gameManag::endGame(int hp) {
    if (hp <= 0) {
        scene->clear();
        uneSec->stop();
        timerPower->stop();
        link->hp = 0;
        QGraphicsPixmapItem *gameOver = new QGraphicsPixmapItem();
        gameOver->setPixmap(QPixmap(":rsrc/img/gameOver2.png"));
        gameOver->setPos(500, 330);
        gameOver->ensureVisible();
        scene->addItem(gameOver);
    }
}

void gameManag::playMusique(){

    musique = new QMediaPlayer();
    musique->setMedia(QUrl("qrc:/rsrc/music/GameIntro.mp3"));
    musique->play();

}



