#include "bullet.h"
#include "hero.h"
#include "ennemi.h"
#include <QDebug>

Bullet::Bullet(int spd, bool right, bool left, bool up,
               bool down)
{

    this->spd = spd;
    this->up = up;
    this->down = down;
    this->right = right;
    this->left = left;

    if (left || right) {
        setPixmap(QPixmap(":rsrc/img/bullet.png"));
    }
    else {
        setPixmap(QPixmap(":rsrc/img/bulletHB.png"));
    }


    //connect
    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));
    timer->start(5);
//    QTimer *timer1 = new QTimer();
//    timer->start(700);
//    QObject::connect(timer1, SIGNAL(timeout()), this, SLOT(colliding()));
}

Bullet::~Bullet() {}

void Bullet::move()
{
    if (up)
    {
        setPos(x(), y()-spd);
        if(pos().y() <= 0)
        {
            scene()->removeItem(this);
            delete this;
        }
    }
    if (down)
    {
        setPos(x(), y()+spd);
        if(pos().y() >= 850)
        {
            scene()->removeItem(this);
            delete this;
        }
    }
    if (left)
    {
        setPos(x()-spd, y());
        if(pos().x() <=0)
        {
           scene()->removeItem(this);
           delete this;
        }
    }
    if (right)
    {
        setPos(x()+spd, y());
        if(pos().x() >= 1350)
        {
           scene()->removeItem(this);
           delete this;
        }
    }
}

//void Bullet::colliding(){
//    QList<QGraphicsItem *> colliding_items = collidingItems();
//    for(int i = 0; i < colliding_items.length(); i++)
//    {
//        ennemi * ennemi1 = dynamic_cast<ennemi *>(colliding_items[i]);
//        hero *hero1 = dynamic_cast<hero *>(colliding_items[i]);


//        if (hero1 || ennemi1){
//            scene()->removeItem(this);
//            delete this;
//        }


//    }
//}

