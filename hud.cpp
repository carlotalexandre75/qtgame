#include "hud.h"
#include <QDebug>
HUD::HUD(QString txt, int posX, int posY, int sizeFont) {
    // initialize the score to 0
    this->value = 0;
    this->txt = txt;
    setZValue(100);
    // draw the text
    setPlainText(txt + QString::number(value));
    setPos(posX, posY);
    setDefaultTextColor(qRgb(237, 70, 9));
    QFontDatabase::addApplicationFont(":/rsrc/img/orange juice 2.0.ttf");
    QFont font = QFont("Orange Juice",sizeFont);
    setFont(font);

}

void HUD::increase(int a){
    value += a;
    setPlainText(txt + QString::number(value));
}
void HUD::decrease(int a){
    value -= a;
    setPlainText(txt + QString::number(value));
}

int HUD::getVal(){
    return value;
}

void HUD::update(int a){
    setPlainText(txt + QString::number(a));
}


