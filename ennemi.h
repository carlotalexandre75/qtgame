#ifndef ENNEMI_H
#define ENNEMI_H

#include <QMediaPlayer>
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QPointF>

class ennemi : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    public:
        int hp, dmg, spd;
        bool right, left, up, down;

        QMediaPlayer *musiqueMort;
        ennemi(int hp, int dmg, int spd,bool right, bool left, bool up, bool down);
        ~ennemi();

        int getSpd() {return spd;}
        double getX() {return pos().x();}
        double getY() {return pos().y();}
        void takeDmg(int dmg);
        void testCollisions();

    public slots:
       bool isDead();
       void attaquer();
       void isColliding();




    signals:
       void hpChanged(int a);





};

#endif // ENNEMI_H
