#include <QDebug>
#include <QTimer>
#include <QGraphicsScene>
#include <QList>

#include "hero.h"
#include "bullet.h"
#include "gamemanag.h"
#include "ennemi.h"

extern gameManag * gameManag;
extern hero * link;

ennemi::ennemi(int hp_, int dmg_, int spd_,bool right, bool left, bool up, bool down)
{
    hp = hp_;
    dmg = dmg_;
    spd = spd_;

    this->right = right;
    this->left = left;
    this->up = up;
    this->down = down;

    QTimer *timer = new QTimer();
    timer->start(700);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(isColliding()));
}

void ennemi::attaquer(){


    if (left){
        setPos(pos().x() + getSpd(), pos().y() );

    }else if (right) {
        setPos(pos().x() - getSpd(), pos().y() );

    }else if (up) {
        setPos(pos().x() , pos().y() + getSpd());

    }else if(down){
        setPos(pos().x() ,pos().y() - getSpd());
    }



    if(pos().x() < 0 ||pos().x() > 1400 || pos().y() < 0 || pos().y() > 900 ){
        scene()->removeItem(this);
        delete this;
    }
}





void ennemi::isColliding(){

    Bullet * bullet = new Bullet(10,left,right,up,down);

    if (left) {
        bullet->setPos(this->getX() +60,this-> getY() +30);

    } else if (right){
        bullet->setPos(this->getX() -30, this->getY() +30);

    } else if (down) {
        bullet->setPos(this->getX() +15,this-> getY() -30);

    } else if(up){
        bullet->setPos(this->getX() +15, this->getY() +80);

    }
    bullet->ensureVisible();
    scene()->addItem(bullet);



    QList<QGraphicsItem *> colliding_items = collidingItems();
    for(int i = 0; i < colliding_items.length(); i++)
    {
        Bullet *bullet = dynamic_cast<Bullet *>(colliding_items[i]);
        hero *hero1 = dynamic_cast<hero *>(colliding_items[i]);
        if (hero1)
        {
            hero1->hp -= this->dmg;
            emit hpChanged(hp);
            scene()->removeItem(bullet);
            delete this;
        }
    }
}





ennemi::~ennemi(){};


void ennemi::takeDmg(int dmg_)
{
    this->hp -= dmg_;
}



bool ennemi::isDead()
{
    if (this->hp <= 0) {
        this->hp = 0;
        delete this;
        return false;
    }
    return true;
}




