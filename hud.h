#ifndef HUD_H
#define HUD_H

#include <QGraphicsTextItem>
#include <QFont>
#include <QFontDatabase>

class HUD: public QGraphicsTextItem{
    Q_OBJECT
    public:
        QString txt;
        HUD(QString txt, int posX, int posY, int sizeFont);

        void decrease(int a);
        void increase(int a);
        int getVal();
        int value;
   public slots:
        void update(int a);
};

#endif // HUD_H
